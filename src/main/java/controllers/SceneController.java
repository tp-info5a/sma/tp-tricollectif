package controllers;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import models.Environnement;
import proportion.Proportion;
import proportion.ProportionMemoire;
import proportion.ProportionVoisinage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class SceneController {
    private final Stage stageStart;
    private final Stage stageVariable;
    private final Stage stageAlgo;

    private Thread thread;

    public SceneController(Stage stageStart, Stage stageVariable, Stage stageAlgo) {
        this.stageStart = stageStart;
        this.stageVariable = stageVariable;
        this.stageAlgo = stageAlgo;
    }

    public Thread getThread() {
        return thread;
    }

    public void selectVariable() {
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(5));
        gridPane.setHgap(5);
        gridPane.setVgap(5);


        Label labelLigne = new Label("nombre de ligne :");
        gridPane.add(labelLigne, 0, 0);

        TextField textFieldLigne = new TextField();
        textFieldLigne.setText("50");
        textFieldLigne.setEditable(true);
        gridPane.add(textFieldLigne, 1, 0);

        Label labelColonne = new Label("nombre de colonne :");
        gridPane.add(labelColonne, 0, 1);

        TextField textFieldColonne = new TextField();
        textFieldColonne.setText("50");
        textFieldColonne.setEditable(true);
        gridPane.add(textFieldColonne, 1, 1);

        Label labelPas = new Label("nombre de pas :");
        gridPane.add(labelPas, 0, 2);

        TextField textFieldPas = new TextField();
        textFieldPas.setText("1");
        textFieldPas.setEditable(true);
        gridPane.add(textFieldPas, 1, 2);

        Label labelAgent = new Label("nombre d'agent :");
        gridPane.add(labelAgent, 0, 3);

        TextField textFieldAgent = new TextField();
        textFieldAgent.setText("20");
        textFieldAgent.setEditable(true);
        gridPane.add(textFieldAgent, 1, 3);

        Label labelK1 = new Label("valeur de k+ :");
        gridPane.add(labelK1, 0, 4);

        TextField textFieldK1 = new TextField();
        textFieldK1.setText("0.1");
        textFieldK1.setEditable(true);
        gridPane.add(textFieldK1, 1, 4);

        Label labelK2 = new Label("valeur de k- :");
        gridPane.add(labelK2, 0, 5);

        TextField textFieldK2 = new TextField();
        textFieldK2.setText("0.3");
        textFieldK2.setEditable(true);
        gridPane.add(textFieldK2, 1, 5);

        Label labelObjetA = new Label("nombre d'objet A :");
        gridPane.add(labelObjetA, 0, 6);

        TextField textFieldObjetA = new TextField();
        textFieldObjetA.setText("200");
        textFieldObjetA.setEditable(true);
        gridPane.add(textFieldObjetA, 1, 6);

        Label labelObjetB = new Label("nombre d'objet B :");
        gridPane.add(labelObjetB, 0, 7);

        TextField textFieldObjetB = new TextField();
        textFieldObjetB.setText("200");
        textFieldObjetB.setEditable(true);
        gridPane.add(textFieldObjetB, 1, 7);

        Label labelErreur = new Label("erreur :");
        gridPane.add(labelErreur, 0, 8);

        TextField textFieldErreur = new TextField();
        textFieldErreur.setText("0.001");
        textFieldErreur.setEditable(true);
        gridPane.add(textFieldErreur, 1, 8);

        Label labelMemoire = new Label("mémoire :");
        gridPane.add(labelMemoire, 0, 9);

        TextField textFieldMemoire = new TextField();
        textFieldMemoire.setText("10");
        textFieldMemoire.setEditable(true);
        gridPane.add(textFieldMemoire, 1, 9);

        Label labelIteration = new Label("Nombre d'itération :");
        gridPane.add(labelIteration, 0, 10);

        TextField textFieldIteration = new TextField();
        textFieldIteration.setText("2000000");
        textFieldIteration.setEditable(true);
        gridPane.add(textFieldIteration, 1, 10);

        Map<String, Proportion> methodesProportionMap = new HashMap<>();
        methodesProportionMap.put("Voisinage", new ProportionVoisinage());
        methodesProportionMap.put("Mémoire", new ProportionMemoire());

        List<String> methodesProportionList = new ArrayList<>(methodesProportionMap.keySet());

        Label labelProportionPrise = new Label("Méthode de calcul pour la prise :");
        gridPane.add(labelProportionPrise, 0, 11);

        AtomicReference<String> nomMethodePrise = new AtomicReference<>();
        ChoiceBox<String> choiceBoxPrise = new ChoiceBox<>();
        choiceBoxPrise.getItems().addAll(methodesProportionList);
        choiceBoxPrise.setValue(methodesProportionList.get(0));
        nomMethodePrise.set(methodesProportionList.get(0));
        choiceBoxPrise.getSelectionModel().selectedIndexProperty().addListener(
                (observable, oldValue, newValue) -> nomMethodePrise.set(methodesProportionList.get(newValue.intValue()))
        );
        gridPane.add(choiceBoxPrise, 1, 11);

        Label labelProportionDepot = new Label("Méthode de calcul pour le depot :");
        gridPane.add(labelProportionDepot, 0, 12);

        AtomicReference<String> nomMethodeDepot = new AtomicReference<>();
        ChoiceBox<String> choiceBoxDepot = new ChoiceBox<>();
        choiceBoxDepot.getItems().addAll(methodesProportionList);
        choiceBoxDepot.setValue(methodesProportionList.get(0));
        nomMethodeDepot.set(methodesProportionList.get(0));
        choiceBoxDepot.getSelectionModel().selectedIndexProperty().addListener(
                (observable, oldValue, newValue) -> nomMethodeDepot.set(methodesProportionList.get(newValue.intValue()))
        );
        gridPane.add(choiceBoxDepot, 1, 12);

        Button btnInit = new Button("Initialiser l'environnement");
        HBox hbBtnInit = new HBox(10);
        hbBtnInit.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnInit.getChildren().add(btnInit);
        gridPane.add(hbBtnInit, 0, 13, 2, 1);

        Button btnAlgo = new Button("Démarrer l'environnement");
        HBox hbBtnAlgo = new HBox(10);
        hbBtnAlgo.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnAlgo.getChildren().add(btnAlgo);
        btnAlgo.setDisable(true);
        gridPane.add(hbBtnAlgo, 0, 14, 2, 1);

        TextField textFieldProgres = new TextField();
        textFieldProgres.setText("");
        textFieldProgres.setEditable(false);
        gridPane.add(textFieldProgres, 2, 14);

        Button btnStop = new Button("Arrêt");
        HBox hbBtnStop = new HBox(10);
        hbBtnStop.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnStop.getChildren().add(btnStop);
        btnStop.setDisable(true);
        gridPane.add(hbBtnStop, 0, 15, 2, 1);

        btnInit.setOnAction(e -> {
            if (this.stageStart != null) this.stageStart.close();
            if (this.stageAlgo != null) this.stageAlgo.close();
            textFieldProgres.setText("");
            this.initEnv(
                    Integer.parseInt(textFieldLigne.getText()),
                    Integer.parseInt(textFieldColonne.getText()),
                    Integer.parseInt(textFieldPas.getText()),
                    Integer.parseInt(textFieldAgent.getText()),
                    Double.parseDouble(textFieldK1.getText()),
                    Double.parseDouble(textFieldK2.getText()),
                    Integer.parseInt(textFieldObjetA.getText()),
                    Integer.parseInt(textFieldObjetB.getText()),
                    Double.parseDouble(textFieldErreur.getText()),
                    Integer.parseInt(textFieldMemoire.getText()),
                    methodesProportionMap.get(nomMethodePrise.get()),
                    methodesProportionMap.get(nomMethodeDepot.get()),
                    Integer.parseInt(textFieldIteration.getText()),
                    textFieldProgres
            );
            btnAlgo.setDisable(false);
            btnStop.setDisable(false);
        });

        btnAlgo.setOnAction(e -> this.thread.start());

        btnStop.setOnAction(e -> {
            if (this.thread != null) this.thread.interrupt();
            btnAlgo.setDisable(true);
            btnStop.setDisable(true);
        });

        this.stageVariable.setScene(new Scene(gridPane, 550, 550));
        this.stageVariable.show();
    }

    private void initEnv(int nbrLigne, int nbrColonne, int nbrPas, int nbrAgent, double k1, double k2,
                         int nbrObjA, int nbrObjB, double nbrErreur, int nbrMemoire,
                         Proportion proportionPrise, Proportion proportionDepot, int maxIteration, TextField textFieldProgres) {

        Environnement env = new Environnement(nbrLigne, nbrColonne, nbrPas, nbrAgent, k1, k2,
                nbrObjA, nbrObjB, nbrErreur, nbrMemoire, proportionPrise, proportionDepot);

        GridPane gridPaneAlgo = new GridPane();
        env.setCasesCircleOnGridPane(gridPaneAlgo);
        env.updateCasesCircle();

        GridPane gridPaneStart = new GridPane();
        env.setStartGridPane(gridPaneStart);

        Action action = new Action(env, maxIteration, textFieldProgres);
        if (this.thread != null && !this.thread.isInterrupted()) {
            this.thread.interrupt();
        }
        this.thread = new Thread(action);

        this.stageStart.setScene(new Scene(gridPaneStart, 500, 500));
        this.stageStart.show();

        this.stageAlgo.setScene(new Scene(gridPaneAlgo, 500, 500));
        this.stageAlgo.show();
    }
}
