package controllers;

import javafx.scene.control.TextField;
import models.Agent;
import models.Environnement;
import models.MethodeEnum;

public class Action implements Runnable{
    private final Environnement env;
    private final Integer maxIteration;
    private final TextField textFieldProgres;

    public Action(Environnement env, Integer maxIteration, TextField textFieldProgres) {
        this.env = env;
        this.maxIteration = maxIteration;
        this.textFieldProgres = textFieldProgres;
    }

    @Override
    public void run() {
        Agent agent;
        int iteration = 0;
        do {
            /* méthodes
             *  MethodeEnum.ALEATOIRE : sélection d'un agent aléatoirement
             *  MethodeEnum.ORDRE : un agent apres l'autre
             */
            agent = env.chooseAgent(MethodeEnum.ORDRE, iteration);
            agent.move(env);
            agent.percevoir(env);
            agent.action(env);

            iteration += 1;

            /*
             * Rafraichissement de l'affichage graphique tous les 0.1 %
             */
            if (iteration%(this.maxIteration/1000) == 0) {
                if (iteration%(this.maxIteration/100) == 0) {
                    this.textFieldProgres.setText(""+100.0*iteration/this.maxIteration + " %");
                }
                this.env.updateCasesCircle();
            }
        } while (iteration < this.maxIteration && !Thread.currentThread().isInterrupted());
    }
}
