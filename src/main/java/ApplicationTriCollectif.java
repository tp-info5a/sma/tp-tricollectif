import controllers.SceneController;
import javafx.application.Application;
import javafx.stage.Stage;

public class ApplicationTriCollectif extends Application {

    private SceneController sceneController;

    @Override
    public void start(Stage primaryStage) {
        Stage startStage = new Stage();
        startStage.setTitle("Start");

        Stage variableStage = new Stage();
        variableStage.setTitle("Variable");

        primaryStage.setTitle("tri collectif");

        this.sceneController = new SceneController(startStage, variableStage, primaryStage);

        this.sceneController.selectVariable();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if (this.sceneController.getThread() != null) {
            this.sceneController.getThread().interrupt();
        }
    }

    /**
     * <b>Ne pas executé cette méthode !!!</b><br/>
     * <b>Executé Main.main() pour lancé l'app avec javafx</b>
     * @param args param
     */
    public static void startApplication(String[] args) {
        launch(args);
    }
}
