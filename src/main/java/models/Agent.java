package models;

import proportion.Proportion;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Agent {
    private String objetCarried;
    private String objetOnCase;
    private String memory;

    private final Map<DirectionEnum, String> directionEnumStringMap;
    private final Integer maxMemoryLength;
    private final Double error;
    private final Proportion proportionCarry;
    private final Proportion proportionDepot;

    private final double k1;
    private final double k2;

    /**
     * @param maxMemoryLength   Taille maximale de la mémoire de l'agent
     * @param error             Taux d'erreur pour la sélection via la mémoire d'un agent
     * @param proportionCarry   Méthode pour calcul la proportion d'objet identique pour une prise
     * @param proportionDepot   Méthode pour calcul la proportion d'objet identique pour un dépôt
     */
    public Agent(Double k1, Double k2, Integer maxMemoryLength, Double error, Proportion proportionCarry, Proportion proportionDepot){
        this.objetCarried = "0";
        this.objetOnCase = "";
        this.directionEnumStringMap = new HashMap<>();

        for (DirectionEnum value : DirectionEnum.values())
            this.directionEnumStringMap.put(value, "0");

        this.memory = "";
        this.maxMemoryLength = maxMemoryLength;
        this.error = error;
        this.proportionCarry = proportionCarry;
        this.proportionDepot = proportionDepot;
        this.k1 = k1;
        this.k2 = k2;
    }

    public String getObjetCarried() {
        return objetCarried;
    }

    public String getObjetOnCase() {
        return objetOnCase;
    }

    public Map<DirectionEnum, String> getDirectionEnumStringMap() {
        return directionEnumStringMap;
    }

    public String getMemory() {
        return memory;
    }

    public Double getError() {
        return error;
    }

    /**
     * Actualise la memoire de l'agent avec l'objet sur la case
     * Si la mémoire est remplis, la plus ancien élément est supprimé
     */
    private void memorize(){
        if(this.memory.length() == this.maxMemoryLength){
            this.memory = memory.substring(1);
        }
        this.memory = this.memory + objetOnCase;
    }

    /**
     * L'agent percoit son environnement proche
     * @param env   environnement que lequel l'agent évolue
     */
    public void percevoir(Environnement env){
        this.objetOnCase = env.percevoir(this, null);
        this.memorize();
        for (DirectionEnum value : DirectionEnum.values())
            this.directionEnumStringMap.replace(value, env.percevoir(this, value));
    }

    /**
     * L'agent agit sur son environnement
     * @param env   environnement que lequel l'agent évolue
     */
    public void action(Environnement env) {
        Random random = new Random();
        double probCarry = 0;
        double probDepot = 0;
        Double f;
        if (!objetOnCase.equals("0") && objetCarried.equals("0")) {
            f = proportionCarry.calculProportion(this, false);
            probCarry = (k1 / (k1 + f)) * (k1 / (k1 + f));
        }
        if (!objetCarried.equals("0") && objetOnCase.equals("0")) {
            f = proportionDepot.calculProportion(this, true);
            probDepot = (f / (k2 + f)) * (f / (k2 + f));
        }

        double tirage = random.nextDouble();
        if (probCarry > 0 && tirage <= probCarry){
            env.carry(this);
            this.objetCarried = this.objetOnCase;
            this.objetOnCase="0";
        }
        if(probDepot > 0 && tirage <= probDepot){
            env.depot(this);
            this.objetOnCase = objetCarried;
            this.objetCarried="0";
        }
    }

    /**
     * L'agent se déplace dans son environnement
     * @param env   environnement que lequel l'agent évolue
     */
    public void move(Environnement env) {
        env.move(this);
    }
}
