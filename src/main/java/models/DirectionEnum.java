package models;

public enum DirectionEnum {
    NORD(0, -1),
    SUD(0, 1),
    EST(1, 0),
    OUEST(-1, 0);

    private final Integer x;
    private final Integer y;

    DirectionEnum(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
