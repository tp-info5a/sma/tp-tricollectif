package models;

import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import proportion.Proportion;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Environnement {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    private final Map<Agent, PositionAgent> agentPositionMap;
    private final List<List<Case>> grille;
    private final Integer pas;

    private final Integer nbrLigne;
    private final Integer nbrColonne;

    /**
     * @param nbrLigne              nombre de ligne de l'environnement (espace2D)
     * @param nbrCol                nombre de colonne de l'environnement (espace2D)
     * @param pas                   distance qu'un agent peut parcourir un 1 tour
     * @param nbrAgent              nombre d'agent dans l'environnement
     * @param nbrA                  nombre d'objet A
     * @param nbrB                  nombre d'objet B
     * @param error                 Taux d'erreur pour la sélection via la mémoire d'un agent
     * @param maxMemoryLengthAgent  Taille max de la mémoire d'un agent
     * @param proportionCarry       Méthode pour calcul la proportion d'objet identique pour une prise
     * @param proportionDepot       Méthode pour calcul la proportion d'objet identique pour un dépôt
     */
    public Environnement(Integer nbrLigne, Integer nbrCol, Integer pas,
                         Integer nbrAgent, Double k1, Double k2, Integer nbrA, Integer nbrB,
                         Double error, Integer maxMemoryLengthAgent,
                         Proportion proportionCarry, Proportion proportionDepot) {
        this.pas = pas;
        this.agentPositionMap = new HashMap<>();
        this.nbrLigne = nbrLigne;
        this.nbrColonne = nbrCol;
        this.grille = new ArrayList<>();
        List<Case> caseList;
        for (int i = 0 ; i < this.nbrColonne ; i++) {
            caseList = IntStream.range(0, this.nbrLigne)
                    .mapToObj((int value) -> new Case())
                    .collect(Collectors.toList());

            this.grille.add(caseList);
        }

        int i = 0;
        while (i < nbrA) {
            Random random = new Random();
            int randomColonne = random.nextInt(this.nbrColonne);
            int randomLigne = random.nextInt(this.nbrLigne);
            Case aCase = this.grille.get(randomColonne).get(randomLigne);
            if (aCase.getValeur().equals("0")) {
                aCase.setValeur("A");
                i++;
            }
        }

        int j = 0;
        while (j < nbrB) {
            Random random = new Random();
            int randomColonne = random.nextInt(this.nbrColonne);
            int randomLigne = random.nextInt(this.nbrLigne);
            Case aCase = this.grille.get(randomColonne).get(randomLigne);
            if (aCase.getValeur().equals("0")) {
                aCase.setValeur("B");
                j++;
            }
        }

        int k = 0;
        while (k < nbrAgent) {
            Random random = new Random();
            int randomColonne = random.nextInt(this.nbrColonne);
            int randomLigne = random.nextInt(this.nbrLigne);
            Case aCase = this.grille.get(randomColonne).get(randomLigne);
            if (aCase.getAgent() == null) {
                Agent agent = new Agent(k1, k2, maxMemoryLengthAgent, error, proportionCarry, proportionDepot);
                aCase.setAgent(agent);
                this.agentPositionMap.put(agent, new PositionAgent(randomColonne, randomLigne));
                k++;
            }
        }
    }

    /**
     * L'agent se déplace dans un direction aléatoire
     * Si cette direction fait sortir l'agent, alors il en choisit une nouvelle
     * @param agent agent à déplacer
     */
    public void move(Agent agent) {
        PositionAgent position = this.agentPositionMap.get(agent);
        Integer x = position.getX();
        Integer y = position.getY();
        Case aCase1 = this.grille.get(x).get(y);
        aCase1.setAgent(null);

        DirectionEnum direction;
        int deplacementX;
        int deplacementY;
        do {
            Random random = new Random();
            direction = DirectionEnum.values()[random.nextInt(4)];
            deplacementX = direction.getX() * this.pas;
            deplacementY = direction.getY() * this.pas;
            position.setX(x + deplacementX);
            position.setY(y + deplacementY);
        }while (position.getX() < 0 || position.getX() >= this.nbrColonne || position.getY() < 0 || position.getY() >= this.nbrLigne);

        this.agentPositionMap.replace(agent, position);
        Case aCase = this.grille.get(position.getX()).get(position.getY());
        aCase.setAgent(agent);
    }

    /**
     * L'agent regarde s'il y a un objet sur une case définit par la direction
     * Si la direction est nulle, l'agent regarde la case sur laquelle il est
     * @param agent     agent qui doit regarder
     * @param direction direction permettant de définir la case à regarder
     * @return Type de l'objet (A ou B). Si la case est vide, alors on retourne 0
     */
    public String percevoir(Agent agent, DirectionEnum direction) {
        PositionAgent position = this.agentPositionMap.get(agent);
        int x = position.getX();
        int y = position.getY();

        if (direction != null) {
            x = x + direction.getX();
            y = y + direction.getY();
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            if (x >= this.nbrColonne) x = this.nbrColonne - 1;
            if (y >= this.nbrLigne) y = this.nbrLigne - 1;
        }

        return this.grille.get(x).get(y).getValeur();
    }

    /**
     * L'agent prend l'objet de la case
     * @param agent agent qui doit faire l'action
     */
    public void carry(Agent agent) {
        PositionAgent position = this.agentPositionMap.get(agent);
        this.grille.get(position.getX()).get(position.getY()).setValeur("0");
    }

    /**
     * L'agent doit déposer l'objet qu'il porte sur la case où il se trouve
     * @param agent agent qui doit faire l'action
     */
    public void depot(Agent agent) {
        PositionAgent position = this.agentPositionMap.get(agent);
        this.grille.get(position.getX()).get(position.getY()).setValeur(agent.getObjetCarried());
    }

    /**
     * Méthode permettant de savoir quel agent va pouvoir bouger
     * @param methode   type de sélection de l'agent
     * @param iteration nombre d'iteration passées
     * @return  agent sélectionné
     */
    public Agent chooseAgent(MethodeEnum methode, Integer iteration) {
        List<Agent> agentList = new ArrayList<>(this.agentPositionMap.keySet());

        switch(methode){
            case ALEATOIRE:
                Random random = new Random();
                return agentList.get(random.nextInt(agentList.size()));

            case ORDRE:
                return agentList.get(iteration % agentList.size());

            default:
                System.out.println("unknown choice method");
                return agentList.get(iteration % agentList.size());
        }
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        // Integer nbrA = 0;
        // ttInteger nbrB = 0;
        for (int j = 0; j < this.nbrColonne ; j++) {
            for (int i = 0; i <this.nbrLigne ; i++) {
                if (this.grille.get(i).get(j).getValeur().equals("A") || (this.grille.get(i).get(j).getAgent() != null && this.grille.get(i).get(j).getAgent().getObjetCarried().equals("A"))) {
                    string.append(ANSI_RED).append("A").append(ANSI_RESET);
                    // nbrA++;
                }else if (this.grille.get(i).get(j).getValeur().equals("B") || (this.grille.get(i).get(j).getAgent() != null && this.grille.get(i).get(j).getAgent().getObjetCarried().equals("B"))) {
                    string.append(ANSI_GREEN).append("B").append(ANSI_RESET);
                    // nbrB++;
                } else {
                    string.append(" ");
                }
                string.append(" ");
            }
            string.append("\n");
        }
        // string.append(ANSI_RED).append("Nbr A : ").append(nbrA).append(ANSI_RESET).append("\n");
        // string.append(ANSI_GREEN).append("Nbr B : ").append(nbrB).append(ANSI_RESET);
        return string.toString();
    }

    /**
     * Met à jour les cercles des case en fonction des objets <br/>
     * <span color="#ff0000">Rouge : Objet A sur une case</span> <br/>
     * <span color="#ffccff">Rose : Objet A sur un agent</span> <br/>
     * <span color="#009900">Vert : Objet B sur une case </span> <br/>
     * <span color="#00ff00">Vert clair : Objet B sur un agent </span> <br/>
     * <span color="#cccccc">Gris : agent ne portant rien</span>
     */
    public void  updateCasesCircle() {
        for (int i = 0; i < this.nbrColonne ; i++) {
            for (int j = 0; j <this.nbrLigne ; j++) {
                if (this.grille.get(i).get(j).getValeur().equals("A")) {
                    this.grille.get(i).get(j).getCircle().setFill(Color.RED);
                }else if (this.grille.get(i).get(j).getAgent() != null && this.grille.get(i).get(j).getAgent().getObjetCarried().equals("A")) {
                    this.grille.get(i).get(j).getCircle().setFill(Color.LIGHTPINK);
                }else if (this.grille.get(i).get(j).getValeur().equals("B")) {
                    this.grille.get(i).get(j).getCircle().setFill(Color.GREEN);
                }else if(this.grille.get(i).get(j).getAgent() != null && this.grille.get(i).get(j).getAgent().getObjetCarried().equals("B")) {
                    this.grille.get(i).get(j).getCircle().setFill(Color.LIGHTGREEN);
                }else if(this.grille.get(i).get(j).getAgent() != null && this.grille.get(i).get(j).getAgent().getObjetCarried().equals("0")) {
                    this.grille.get(i).get(j).getCircle().setFill(Color.GREY);
                }else {
                    this.grille.get(i).get(j).getCircle().setFill(Color.WHITE);
                }
            }
        }
    }

    /**
     * Place les cercles des case dans le layout de l'interface graphique
     * @param gridPane  layout de l'interface graphique
     */
    public void setCasesCircleOnGridPane(GridPane gridPane) {
        for (int i = 0; i < this.nbrColonne ; i++) {
            for (int j = 0; j <this.nbrLigne ; j++) {
                gridPane.add(this.grille.get(i).get(j).getCircle(), i, j, 1, 1);
            }
        }
    }

    /**
     * Copie de l'état de départ de l'environnement pour avoir un avant/après
     * @param gridPane  layout de l'interface graphique
     */
    public void setStartGridPane(GridPane gridPane) {
        for (int i = 0; i < this.nbrColonne ; i++) {
            for (int j = 0; j <this.nbrLigne ; j++) {
                Circle circle = new Circle(
                        this.grille.get(i).get(j).getCircle().getRadius(),
                        this.grille.get(i).get(j).getCircle().getFill()
                );
                gridPane.add(circle, i, j, 1, 1);
            }
        }
    }
}
