package models;

import javafx.scene.shape.Circle;

public class Case {
    private Agent agent;
    private String valeur;
    private final Circle circle;

    public Case() {
        this.agent = null;
        this.valeur = "0";
        this.circle = new Circle(5);
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Circle getCircle() {
        return circle;
    }

    @Override
    public String toString() {
        return valeur;
    }
}
