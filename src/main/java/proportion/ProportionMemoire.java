package proportion;

import models.Agent;

public class ProportionMemoire implements Proportion{

    public ProportionMemoire() {}

    /**
     * Trouve la proportion d'objet identique dans la mémoire de l'agent
     * @param agent         Agent pour lequel calculer la proportion
     * @param deposerAction Booléen pour savoir si c'est une prise ou un dépôt d'objet
     * @return proportion d'objet identique dans la mémoire
     */
    @Override
    public Double calculProportion(Agent agent, Boolean deposerAction) {
        double occ = 0;
        double diff = 0;
        String obj = deposerAction ? agent.getObjetCarried() : agent.getObjetOnCase();

        for(int i =0; i< agent.getMemory().length(); i++) {
            if(agent.getMemory().charAt(i) != obj.charAt(0) && agent.getMemory().charAt(i) != '0'){
                diff++;
            }else if(agent.getMemory().charAt(i) == obj.charAt(0)){
                occ++;
            }
        }
        return (occ + diff * agent.getError())/agent.getMemory().length();
    }
}
