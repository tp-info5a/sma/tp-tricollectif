package proportion;

import models.Agent;

public interface Proportion {
    Double calculProportion(Agent agent, Boolean deposerAction);
}
