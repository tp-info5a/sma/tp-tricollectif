package proportion;

import models.Agent;
import models.DirectionEnum;

public class ProportionVoisinage implements Proportion {

    public ProportionVoisinage() {}

    /**
     * Trouve la proportion d'objet identique dans le voisinage de l'agent
     * @param agent         Agent pour lequel calculer la proportion
     * @param deposerAction Booléen pour savoir si c'est une prise ou un dépôt d'objet
     * @return proportion d'objet identique dans le voisinage
     */
    @Override
    public Double calculProportion(Agent agent, Boolean deposerAction) {
        double nbObjSameType = 0;
        String obj = deposerAction ? agent.getObjetCarried() : agent.getObjetOnCase();

        for (DirectionEnum value : agent.getDirectionEnumStringMap().keySet())
            if(agent.getDirectionEnumStringMap().get(value).equals(obj)) nbObjSameType++;

        return nbObjSameType/agent.getDirectionEnumStringMap().keySet().size();
    }
}
