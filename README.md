# TP SMA : tri collectif

## Elèves

Cazet Martin & ARMANET Nathan

## Contexte

Ce projet est réalisé dans le cadre de l'UE SMA de 5ème année d'informatique à Polytech Lyon

## Description

Il s'agit d'un SMA (Système Multi-Agents) simulant le comportement des fourmies.
Les agents trient 2 types d'objet (*A* et *B*). Le but de ces agents est de parcourir l'environnement,
ils ont une certaine probabilité de prendre un objet lorsqu'il passe dessus
et un certaine probabilité de le posé plus loin.

Ces règles simples permettent de trier les *objets A* des *objets B*.

### Affichage

On peut modifier les paramètres de l'environnement via la fenêtre  **"Variable"**. Cette fenêtre permet aussi de lancé et/ou arréter la simulation.

La fenêtre **"Start"** permet de voir l'état initiale de la simulation, elle sera utile pour faire une comparaison avec l'état actuel/final de celle-ci.

La fenêtre **"Tri Collectif"** permet de voir l'état actuel puis final de la simulation.

### Représentation

Chaque case de l'environnement est représenté par un cercle dont la couleur varie en fonction de l'état de la case :

- Blanche : la case est vide
- Rouge : Un objet A est sur la case
- Vert : Un objet B est sur la case
- Gris : Un agent ne portant rien est sur la case
- Rouge clair : Un agent portant un objet A est sur la case
- Vert clair : Un agent portant un objet B est sur la case
